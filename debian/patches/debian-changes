The Debian packaging of emacs-doom-themes is maintained in git, using the
merging workflow described in dgit-maint-merge(7).  There isn't a
patch queue that can be represented as a quilt series.

A detailed breakdown of the changes is available from their canonical
representation - git commits in the packaging repository.  For
example, to see the changes made by the Debian maintainer in the first
upload of upstream version 1.2.3, you could use:

    % git clone https://git.dgit.debian.org/emacs-doom-themes
    % cd emacs-doom-themes
    % git log --oneline 1.2.3..debian/1.2.3-1 -- . ':!debian'

(If you have dgit, use `dgit clone emacs-doom-themes`, rather than plain
`git clone`.)

A single combined diff, containing all the changes, follows.
--- emacs-doom-themes-2.1.6+git20210505.4d24728.orig/README.md
+++ emacs-doom-themes-2.1.6+git20210505.4d24728/README.md
@@ -85,12 +85,6 @@ A list of themes which are usable, yet m
 
 - `(doom-themes-visual-bell-config)`: flash the mode-line when the Emacs bell
   rings (i.e. an error occurs).
-- `(doom-themes-neotree-config)`: a [neotree] theme that takes after [Atom]'s
-  file drawer, and is simpler than the built in icon theme in neotree
-  ([screenshot](/../screenshots/doom-one.png), [more details][wiki]).
-
-  This requires `all-the-icons`' fonts to be installed: `M-x
-  all-the-icons-install-fonts`
 - `(doom-themes-treemacs-config)`: two [treemacs] icon themes, one that takes after
   [Atom]'s, and a second more colorful implementation (WIP).
 - `(doom-themes-org-config)`: corrects and improves some of org-mode's native
@@ -158,12 +152,6 @@ Here is a example configuration for `doo
   ;; Enable flashing mode-line on errors
   (doom-themes-visual-bell-config)
   
-  ;; Enable custom neotree theme (all-the-icons must be installed!)
-  (doom-themes-neotree-config)
-  ;; or for treemacs users
-  (setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
-  (doom-themes-treemacs-config)
-  
   ;; Corrects (and improves) org-mode's native fontification.
   (doom-themes-org-config))
 ```
